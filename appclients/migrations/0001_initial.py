# Generated by Django 3.2 on 2022-02-05 10:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='Имя')),
                ('phone', models.PositiveBigIntegerField(verbose_name='Номер телефона')),
                ('code', models.PositiveSmallIntegerField(db_index=True, verbose_name='Код мобильного оператора')),
                ('tag', models.CharField(db_index=True, max_length=64, verbose_name='Тег')),
                ('timezone', models.SmallIntegerField(default=3, verbose_name='Часовой пояс')),
            ],
        ),
    ]
