from django.contrib.auth import get_user_model
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not get_user_model().objects.filter(username='test_su').exists():
            get_user_model().objects.create_superuser(username='test_su', password='rasras12',
                                                      first_name='test', last_name='su')
            print('Standard super user created')
        else:
            print('Standard super user is exists, or standard super user email is already exists')
