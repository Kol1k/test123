from django.contrib import admin

from appnotifications.models import Message, Notify

admin.site.register(Notify)
admin.site.register(Message)
