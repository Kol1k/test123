from datetime import datetime

from django.db import models

from appclients.models import Client
from notifyservice.convert_time import convert_time, get_tz


class Notify(models.Model):
    start = models.DateTimeField('Дата-время начала рассылки')
    stop = models.DateTimeField('Дата-время прекращения рассылки')
    message = models.CharField('Текст сообщения', max_length=256)
    code_filter = models.PositiveSmallIntegerField('Фильтр адресатов по коду оператора', default=0)
    tag_filter = models.CharField('Фильтр адресатов по тегу', max_length=64, blank=True)
    updated = models.DateTimeField('Дата-время изменения параметров рассылки', auto_now=True)

    @property
    def messages_stats(self):
        """Подсчёт статистики рассылки с разбивкой по статусам"""
        now = datetime.now(get_tz())
        start = convert_time(self.start)
        if now > start:
            messages = self.messages
            statuses = Message.STATUS_CHOICES   # не очень красиво, но других способов в голову не пришло
            summary = {'Всего сообщений': messages.count()}
            for status_code, status_name in statuses:
                msg_with_status_count = messages.filter(status=status_code).count()
                summary[status_name] = msg_with_status_count
            return summary
        return {}

    class Meta:
        verbose_name_plural = 'Notifications'


class Message(models.Model):
    ABORTED = 'A'
    ERROR = 'E'
    RESENDING = 'R'
    SENT = 'S'
    WAIT = 'W'

    STATUS_CHOICES = (
        (ABORTED, 'Отправка отменена'),
        (ERROR, 'Не удалось отправить сообщение в срок'),
        (RESENDING, 'Повторная отправка'),
        (SENT, 'Успешно отправлено'),
        (WAIT, 'Ожидание первой отправки'),
    )

    added = models.DateTimeField('Дата-время создания', auto_now_add=True)
    status = models.CharField('Статус отправки', max_length=1, choices=STATUS_CHOICES, default='W', db_index=True)
    notify = models.ForeignKey(Notify, on_delete=models.CASCADE, related_name='messages', db_index=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='messages')

    @property
    def verbose_status(self):
        return self.get_status_display()

    class Meta:
        ordering = ('status', 'client', )
