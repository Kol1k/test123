import pytz
from dateutil.parser import parse

from notifyservice.settings import TIME_ZONE


def get_tz():
    return pytz.timezone(TIME_ZONE)


def convert_time(dt_field):
    return parse(str(dt_field))


def str_to_datetime(datetime_str):
    return parse(datetime_str)
